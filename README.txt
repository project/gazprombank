CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------
Gazprombank module provides integration of the Commerce module and Gazprombank
payment API. It makes available new payment method for Commerce module to allow
users pay using Gazprombank.

REQUIREMENTS
------------
This module requires:
* Drupal commerce module https://www.drupal.org/project/commerce
* Commerce payment method module (Included into Drupal commerce)
* OpenSSL extension for PHP. Gazprombank needs a verification via OpenSSL.

INSTALLATION
------------
Install as you would normally install a contributed Drupal module. See:
https://drupal.org/documentation/install/modules-themes/modules-7 for further
information.

CONFIGURATION
------------
All configuration is available via payment method configuration.

Additional configuration (paths configuration) located in the settings
at the "Services" menu ( admin/config/services/gazprombank )

MAINTAINERS
-----------
* Yaroslav Lushnikov (zvse) - https://www.drupal.org/user/2870933
